#include <stdio.h>

int main() {
	
	double nc;                       // defined outside loop for scopeing purposes
	for(nc=0; getchar()!=EOF; ++nc); // notice the loop has no body!
									 // the isolated ; is a "null statement"
									 // equivilant to {}
	printf("%.0f", nc);              // executes on program termination (ctrl+c ?= EOF)
}