#include <stdio.h> 								        // standard input/output

#define UPPER 300                                       // magic numbers are bad, defining SYMBOLIC CONSTANTS good :)
#define LOWER 0                                         // remember, no semicolon on defines - they are a preproccessor directive
#define STEP  20                                        // directives are not programming statements per se, but rather directives for the preprocessor

int main() {
	printf("degC\tdegF\n"); 					        // key
	for(int i=UPPER;i>=LOWER;i-=STEP) 	     	        // iterate the table
		printf("%3d%9.1f\n", i, (9./5.)*i+32.); 		// print the values in the table
}