#include <stdio.h>

// echo input to output, but make hidden characters visible

int main() {
	int c;

	while((c = getchar()) != EOF) {
		     if(c == '\t')
			printf("\\t");
		else if(c == '\b') //note you cant test this on windows http://bit.ly/1zhmvU2
			printf("\\b");
		else if(c == '\\')
			printf("\\\\");
		else
			putchar(c);
	}
	printf("done\n");
}