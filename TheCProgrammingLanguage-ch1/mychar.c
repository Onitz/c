#include <stdio.h>

int main() {
	int mychar = 66; //todays letter is 'B'

	printf("%c\n", mychar);
	printf("The number for a newline character is: %d\n", '\n');
	printf("The number for EOF is %d\n", EOF);
}