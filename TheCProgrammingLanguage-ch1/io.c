#include <stdio.h>

int main(){
	int c;            //remember, c being loosly typed can easily convert between ints/chars ;)
	c = getchar();
	while(c != EOF) {    //this program iterates through all the characters of an input and echos it as the user types it

		putchar(c);
		c = getchar();
	}
}