#include <stdio.h>

//write a program that prints its input 1 word per line
// you may like to rewrite it so it merges newline characters, but good for now

int main() {

	int c;
	while((c = getchar()) != EOF)
		if(c==' ' || c=='\t' || c=='\n') //the if-else block is inherently linked and consequently appears to be treated as a single statement by the while loop :)
			putchar('\n');
		else
			putchar(c);

}