#include <stdio.h>

//this program counts the number of input lines
//note that being loosly typed, c can automatically convert between chars and ints

int main() {

	int c, nl=0; //character, no.lines

	while((c = getchar()) != EOF)
		if(c=='\n') //notice, c was initially defined as an int
			++nl;

	printf("%d\n", nl);

}