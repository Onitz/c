#include <stdio.h>

int main() {
	int c;
	c = getchar();
	//while((c = getchar()) != EOF) 
	//	putchar(c);

	//printf(EOF);  //error is "invalid conversion from 'int' to 'const char*'"
	//putchar(EOF); //fine
	//printf("%d\n", EOF); //fine
	//printf("%a", EOF);   //hex representation
	printf("%d\t%d", (c==EOF), (c!=EOF));
}