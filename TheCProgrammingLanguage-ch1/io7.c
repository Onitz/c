//replace 2 or more spaces with a single space, echo everything else
#include <stdio.h>

int main() {
	int c, d;

	while((c = getchar()) != EOF) {
		putchar(c);
		if(c==' ') {
			while((d = getchar()) == ' ') // do nothing while we have extra spaces
				;                         // null statement
			putchar(d); 				  // when the inner loop fails, it will leave 1 character unaccounted for ;)

		}
	}

	printf("done :)\n");
}	