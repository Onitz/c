#include <stdio.h>

//to ensure the whole program executes in a windows command prompt, you have to pipe:
//
// 			arrays1 < input.txt
//
/*count digits, white space, others */
int main() {
	int c, i, nwhite, nother;
	int ndigit[10];

	nwhite = nother = 0; //initialise

	for(i=0;i<10;++i)
		ndigit[i] = 0;

	while ((c=getchar()) != EOF)
		if(c>='0' && c<='9')                
			++ndigit[c-'0'];                    //c-'0' is the ascii value 48-58 of a digit converted to 0-9 ..quite clever subtracting a char from an int
		else if(c==' '||c=='\n'||c=='\t')
			++nwhite;
		else
			++nother;

	printf("\n\ndigits = ");
	for(i = 0; i<10; ++i)
		printf("%d ", ndigit[i]);
	printf(", whitespace = %d, other = %d\n", nwhite, nother);
}