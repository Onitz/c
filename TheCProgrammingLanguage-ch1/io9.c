#include <stdio.h>

//counting words

#define IN 1
#define OUT 0 //remember, no semicolons ;)

int main() { /* count words, lines, characters and inputs */
	int c, nl, nw, nc, state;

	state = OUT;      //the state variable counts weather a program is in a word or not
	nl = nw = nc = 0; //ooh, thats really cool, didn't know you could do that
	while ((c = getchar()) != EOF) {
		++nc;
		if(c=='\n')
			++nl;
		if(c==' ' || c=='\n' || c=='\t')
			state = OUT;
		else if(state == OUT) {
			state = IN;
			++nw;
		}
	}
	printf("%d %d %d\n", nl, nw, nc);
}