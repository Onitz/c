//write a program to count blanks, tabs and newlines

#include <stdio.h>

int main() {
	int b=0, t=0, n=0, c;

	while((c=getchar()) != EOF) {
		if(c==' ')
			++b;
		else if(c=='\t')
			++t;
		else if(c=='\n')
			++n;
	}

	printf("\nBlanks:\t\t%d\nTabs:\t\t%d\nNewlines:\t%d\n", b, t, n);
	//it only executes one statement
	//this method seems failry restrictive..
}