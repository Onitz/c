#include <stdio.h>

int main() {
	int c;                          // the character. C is weakly typed.
	while((c=getchar()) != EOF)
		putchar(c);
	printf("\nEnd.");
}