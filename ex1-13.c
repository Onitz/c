#include <stdio.h>
#define MAXLEN 20                                              // number of values to count in our histogram

int amax(int a[], int alen) {                                  // simple max function, attempting to implicitly determine array length: alen was far too much of a headache
	if(alen<2)                                                 // will return garbage if alen=0, first element if alen=1
		return a[0];
	int max = a[0];                                            // initialise max

	for(int i=1;i<alen;i++)                                    // O(n), get the largest element of an unsorted array
		if(max < a[i])
			max = a[i];

	return max;                                                // return the maximum element in the array
}

int main() {
	printf("Word counter, delimited by \".\"\n");              // instruct the user

	int c;
	int counter = 0;                                           // temp var for registering strlens
	int count[MAXLEN];                                         // the number of times a word of lenght count[i] occurs
	for(int i=0;i<MAXLEN;i++)                                  // initialise
		count[i]=0;

	while((c=getchar()) != '.') { 
		if(c != ' ' && c!='\t' && c!='\n' && c!=EOF)           // if its not a whitespace, increment counter
			++counter;

		else if(counter != 0) {                                // otherwise, if its not a repeating whitespace, register & reset
			count[(counter-1) % MAXLEN]++;                     // minus 1 because the zeroth array element stores words of lenght 1
			counter=0;                                         // printf("registerd %i\n", counter);
		} 
	}                                                          // the perfect scenario where a do-while makes sense, but it doesnt work for som resn
	if(counter!=0)                                             // account for last character
		count[(counter-1) % MAXLEN]++;
	int max = amax(count, MAXLEN);	                           // histogram will count up to 20 characters

	for(int j=max;j>=0;j--) {                                  // rows of histogram
		putchar('\n');
		for(int i=0;i<MAXLEN;i++) {			                   // cols of histogram putchar(count[i]+'0'); 
			putchar(j<count[i]? '*' : ' ');                    // remember! if we putchar(count[i]) we're actually just putting the character ascii#0 (int 0 = invisible char)
			putchar(' ');
		}
	}

	printf("\n\n1 2 3 4 5 6 7 8 9 a b c d e f g h i j k");     // histogram legend
	printf("\n\nEnd.\n");
	getchar();                                                 // keep the window open if not already running in a terminal (Windows machines)
	getchar();
}